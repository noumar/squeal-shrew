{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE EmptyDataDecls #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

module Main where

import qualified Ckan
import qualified DublinCore as DC
import qualified Solr

import Control.Lens ((&), (.~), (?~))
import Control.Monad.IO.Class (liftIO)
import Data.Aeson (FromJSON, ToJSON)
import qualified Data.ByteString.Builder as BB
import qualified Data.ByteString.Char8 as BS
import qualified Data.ByteString.Lazy.Char8 as BL
import qualified Data.Conduit as C
import Data.Maybe (fromJust, fromMaybe, mapMaybe)
import qualified Data.Swagger as SW
import qualified Data.Text as TX
import qualified Data.Time.Clock as TC
import qualified Data.Time.Format as TF
import qualified Data.UUID.Types as UUID (UUID, toString)
import qualified Data.UUID.V4 as UUID (nextRandom)
import GHC.Generics (Generic)
import qualified Network.HTTP.Simple as H
import qualified Network.HTTP.Types as T
import qualified Network.Wai as W
import qualified Network.Wai.Conduit as WC
import Network.Wai.Handler.Warp (defaultSettings, runSettings, setPort, setServerName)
import qualified Network.Wai.Middleware.RequestLogger as WL
import Servant
import qualified Servant.Mock as SM
import qualified Servant.Server.Internal.Router as R
import qualified Servant.Server.Internal.RoutingApplication as RA
import qualified Servant.Swagger as SSW
import qualified Servant.Swagger.UI as SSWU
import qualified Test.QuickCheck as QC
import Test.QuickCheck.Instances ()
import Text.Read (readMaybe)
import qualified Text.XML as X
import Text.XML.Cursor
       (Cursor, ($/), (&/), (>=>), content, element, fromDocument)

{-# ANN module ("HLint: ignore Use camelCase" :: String) #-}


instance SW.ToSchema DigitalObjectID

instance SW.ToParamSchema DigitalObjectID

instance SW.ToSchema EntityID

instance SW.ToParamSchema EntityID

instance SW.ToSchema SchemaID

instance SW.ToParamSchema SchemaID

instance SW.ToSchema UserID

instance SW.ToParamSchema UserID

instance SW.ToSchema CommunityID

instance SW.ToParamSchema CommunityID

instance SW.ToSchema DigitalObject

instance QC.Arbitrary DigitalObject where
  arbitrary =
    DigitalObject <$> QC.arbitrary <*> QC.arbitrary <*> QC.arbitrary <*>
    QC.arbitrary

instance SW.ToSchema Metadata

instance QC.Arbitrary Metadata where
  arbitrary = Metadata <$> QC.arbitrary <*> QC.arbitrary

instance SW.ToSchema File where
  declareNamedSchema _ = return $ SW.NamedSchema (Just "File") SW.binarySchema

instance SW.ToSchema Community

instance QC.Arbitrary Community where
  arbitrary =
    Community <$> QC.arbitrary <*> QC.arbitrary <*> QC.arbitrary <*>
    QC.arbitrary

instance SW.ToSchema Schema

instance QC.Arbitrary Schema where
  arbitrary =
    Schema <$> QC.arbitrary <*> QC.arbitrary <*> QC.arbitrary <*> QC.arbitrary <*>
    QC.arbitrary

instance SW.ToSchema Field

instance QC.Arbitrary Field where
  arbitrary =
    Field <$> QC.arbitrary <*> QC.arbitrary <*> QC.arbitrary <*> QC.arbitrary <*>
    QC.arbitrary <*>
    QC.arbitrary

instance SW.ToSchema RoleID

instance SW.ToSchema User

instance QC.Arbitrary User where
  arbitrary = User <$> QC.arbitrary <*> QC.arbitrary <*> QC.arbitrary

newtype CommunityID =
  CommunityID UUID.UUID
  deriving stock (Show, Generic)
  deriving newtype (ToJSON, FromJSON, FromHttpApiData, QC.Arbitrary)

newtype DigitalObjectID =
  DigitalObjectID UUID.UUID
  deriving stock (Show, Generic)
  deriving newtype (FromJSON, ToJSON, FromHttpApiData, QC.Arbitrary)

newtype EntityID =
  EntityID Int
  deriving stock (Show, Generic)
  deriving newtype (ToJSON, FromHttpApiData, QC.Arbitrary)

newtype File =
  File BL.ByteString
  deriving newtype (MimeUnrender OctetStream, MimeRender OctetStream, QC.Arbitrary)

newtype RoleID =
  RoleID Int
  deriving stock (Show, Generic)
  deriving newtype (QC.Arbitrary, ToJSON, FromJSON)

newtype SchemaID =
  SchemaID Int
  deriving stock (Show, Generic)
  deriving newtype (FromJSON, ToJSON, FromHttpApiData, QC.Arbitrary)

newtype UserID =
  UserID Int
  deriving stock (Show, Generic)
  deriving newtype (FromJSON, ToJSON, FromHttpApiData, QC.Arbitrary)

data Role = Role
  { id :: RoleID
  , name :: String
  , community :: CommunityID
  }

data Field = Field
  { name :: String
  , type' :: String
  , description :: String
  , required :: Bool
  , cardinality :: Int
  , controlled_vocabulary :: [String] -- list of allowed values
  }
  deriving stock (Generic, Show)
  deriving anyclass (ToJSON, FromJSON)

data Schema = Schema
  { id :: SchemaID
  , managed_by :: RoleID
  , is_deprecated :: Bool
  , new_version :: SchemaID
  , fields :: [Field]
  }
  deriving stock (Generic, Show)
  deriving anyclass (ToJSON, FromJSON)

data Metadata = Metadata
  { schema :: SchemaID
  , fields :: DC.DublinCore
  }
  deriving stock (Generic, Show)
  deriving anyclass (ToJSON, FromJSON)

data DigitalObject = DigitalObject
  { id :: DigitalObjectID
  , published_by :: UserID
  , files_count :: Int
  , metadata :: Metadata
  }
  deriving stock (Generic, Show)
  deriving anyclass (ToJSON, FromJSON)

data Community = Community
  { id :: CommunityID
  , name :: String
  , desc :: String
  , schemas :: [SchemaID]
  }
  deriving stock (Generic, Show)
  deriving anyclass (ToJSON, FromJSON)

data User = User
  { id :: UserID
  , name :: String
  , email :: String
  }
  deriving stock (Generic, Show)
  deriving anyclass (ToJSON)

type API
   = SSWU.SwaggerSchemaUI "swagger-ui" "swagger.json" :<|> BaseAPI :> ExtendedEudatAPI --BasicEudatAPI

type BaseAPI = "api"

type SecureEudatAPI
   = Header "Authorization" String :> BasicEudatAPI :<|> Header "Authorization" String :> ExtendedEudatAPI

type BasicEudatAPI
  -- Digital Objects
   = "digitalobjects" :> QueryParam "search" String :> QueryParam "size" Int :> QueryParam "from" Int :> Get '[ JSON] (Headers '[ Header "X-Total" Int, Header "X-Start" Int] [DigitalObjectID]) -- Search; {[“id”: “<ID>”, . . . ] }
      :<|> "digitalobjects" :> ReqBody '[ JSON] DigitalObject :> Post '[ JSON] DigitalObjectID -- Create a Digital Object; { “id”: “<ID>” }
      :<|> "digitalobjects" :> Capture "oid" DigitalObjectID :> Get '[ JSON] DigitalObject -- Retrieve a Digital Object; {“id”: “<ID>”, “published_by”: “<USER_ID>”, “files count”: “42”, “metadata”: [ { “schema”: “<SCHEMA_ID>”, “fields”: {} } ] }
      :<|> "digitalobjects" :> Capture "oid" DigitalObjectID :> ReqBody '[ JSON] DigitalObject :> Patch '[ PlainText] NoContent -- Change state of a Digital Object; HTTP-code
  --  Digital Entities (Files)
      :<|> "digitalobjects" :> Capture "oid" DigitalObjectID :> "entities" :> ReqBody '[ OctetStream] File :> Header "X-Filename" String :> Post '[ JSON] EntityID -- Add a Digital Entity (a file) to a Digital Object; status message with name and size of the uploaded file
      :<|> "digitalobjects" :> Capture "oid" DigitalObjectID :> "entities" :> QueryParam "parent_id" DigitalObjectID -- id of a Digital Object to search
      :> QueryParam "recursive" String -- list all the entities in the Digital Object and child Digital Objects recursively
      :> QueryParam "name" String -- file name
      :> Get '[ JSON] [EntityID] -- Search for an entity or a set of entities in a Digital Object; { “id”: “<ENTITY_ID>” }
      :<|> "digitalobjects" :> Capture "oid" DigitalObjectID :> "entities" :> Capture "eid" EntityID :> Get '[ OctetStream] File -- Retrieve an entity (file); physical file
      :<|> "digitalobjects" :> Capture "oid" DigitalObjectID :> "entities" :> Capture "eid" EntityID :> Delete '[ PlainText] NoContent -- Delete an entity (file); HTTP-code
      :<|> "digitalobjects" :> Capture "oid" DigitalObjectID :> "entities" :> Capture "eid" EntityID :> Patch '[ PlainText] NoContent -- Change file name; HTTP-code

type ExtendedEudatAPI
   = BasicEudatAPI
  -- Communities
      :<|> "communities" :> Get '[ JSON] [CommunityID] -- Search and retrieve communities; list of communities with community information / JSON array of objects
      :<|> "communities" :> ReqBody '[ JSON] Community :> Post '[ JSON] CommunityID -- Create a community; list of communities with community information / { “id”: “<COMMUNITY ID>” }
      :<|> "communities" :> Capture "cid" CommunityID :> Get '[ JSON] Community -- Retrieve information for a specific community; community information / { “id”: “<COMMUNITY ID>”, “name”: “. . . ”, “desc”: “. . . ”, “schemas”: [ <SCHEMA ID> ] }
      :<|> "communities" :> Capture "cid" CommunityID :> Patch '[ JSON] Community -- Modify information for a specific community; community information / { “id”: “<COMMUNITY ID>”, “name”: “. . . ”, “desc”: “. . . ”, “schemas”: [ <SCHEMA ID> ] }
      :<|> "communities" :> Capture "cid" CommunityID :> "digitalobjects" :> QueryParam "size" Int -- retrieve 'size' number of items
      :> QueryParam "from" Int -- from starting point 'from'
      :> Get '[ JSON] (Headers '[ Header "X-Total" Int] [DigitalObjectID]) -- List all Digital Objects for a community; list of objects belonging to the community [{ ... }, { ... }]
  -- Schemas
      :<|> "schemas" :> Get '[ JSON] [SchemaID] -- Search/list community schemas; list of schemas / JSON object
      :<|> "schemas" :> ReqBody '[ JSON] Schema :> Post '[ JSON] SchemaID -- Create a new community schema; new schema / JSON object
      :<|> "schemas" :> Capture "sid" SchemaID :> Get '[ JSON] Schema -- Retrieve a specific schema; schema information / JSON object
      :<|> "schemas" :> Capture "sid" SchemaID :> "deprecate" :> QueryParam "new_version" SchemaID :> ReqBody '[ JSON] Schema :> Post '[ JSON] Schema -- Deprecate and (optionally) replace a schema; schema information / { “id”: “<SCHEMA ID>”, “managed by”: “<ROLE>”, “is deprecated”: “true/false”, “new version”: “<SCHEMA ID>”, “fields”: [ { name: “. . . ”, “type”: “. . . ”, “desc”: “. . . ”, “required”: “true/false”, “cardinality”: “34” , “controlled vocabulary”: [list of allowed values] } ] }
  -- User management
      :<|> "users" :> Get '[ JSON] (Headers '[ Header "X-Total" Int] [UserID]) -- list of users profile information; [ { “id”: <USER ID>, “name”: “. . . ”, “email”: “...”},... ]
      :<|> "users" :> Capture "uid" UserID :> Get '[ JSON] User -- users profile information for user; { “id”: <USER ID>, “name”: “. . . ”, “email”: “. . . ” }

eudatAPI :: Proxy ExtendedEudatAPI --BasicEudatAPI
eudatAPI = Proxy

eudatSwaggerAPI :: Proxy API
eudatSwaggerAPI = Proxy

eudatApp :: Application
eudatApp = WL.logStdoutDev $ serve eudatSwaggerAPI eudatServer

mockEudatApp :: Application
mockEudatApp = WL.logStdoutDev $ serve eudatAPI (SM.mock eudatAPI Proxy)

eudatServer :: Server API
eudatServer =
  SSWU.swaggerSchemaUIServer
    (SSW.toSwagger eudatAPI & SW.info . SW.title .~ "EUDAT API" & SW.info .
     SW.version .~
     "2016.11.28" &
     SW.info .
     SW.description ?~
     "EUDAT CDI HTTP REST API" &
     SW.basePath ?~
     "/api") :<|>
  basicEudatAPI :<|>
  extendedEudatAPI

basicEudatAPI =
  getDigitalObjectIDs :<|> putDigitalObject :<|> getDigitalObject :<|>
  (\_ _ -> throwError err501) :<|>
  (\_ _ _ -> throwError err501) :<|>
  (\_ _ _ _ -> throwError err501) :<|>
  (\_ _ -> throwError err501) :<|>
  (\_ _ -> throwError err501) :<|>
  (\_ _ -> throwError err501)
  where
    getDigitalObjectIDs ::
         Maybe String
      -> Maybe Int
      -> Maybe Int
      -> Handler (Headers '[ Header "X-Total" Int, Header "X-Start" Int] [DigitalObjectID])
    getDigitalObjectIDs search size from = do
      res <-
        H.httpJSON
          (H.setRequestQueryString
             [ ("wt", Just "json")
             , ("echoParams", Just "none")
             , ("q", Just "*:*")
             , ("fq", Just $ maybe "*:*" BS.pack search)
             , ("fl", Just "id")
             , ("start", Just $ maybe "0" (BS.pack . show) from)
             , ("rows", Just $ maybe "10" (BS.pack . show) size)
             ]
             req)
      case Solr.response (H.getResponseBody res) of
        Just x ->
          return $ addHeader (Solr.numFound x) $ addHeader (Solr.start x) $
          map DigitalObjectID $
          mapMaybe Solr.id (Solr.docs x)
        Nothing -> throwError err400
    putDigitalObject :: DigitalObject -> Handler DigitalObjectID
    putDigitalObject obj = do
      liftIO (print obj)
      oid <- liftIO UUID.nextRandom
      return $ DigitalObjectID oid
    getDigitalObject :: DigitalObjectID -> Handler DigitalObject
    getDigitalObject (DigitalObjectID id') = do
      res <-
        H.httpJSON
          (H.setRequestQueryString
             [ ("wt", Just "json")
             , ("echoParams", Just "none")
             , ("q", Just "*:*")
             , ("fq", Just $ BS.pack $ "id:" ++ UUID.toString id')
             ]
             req)
      case Solr.response (H.getResponseBody res) of
        Just x ->
          if Solr.numFound x /= 1
            then throwError err404
            else let doc = head $ Solr.docs x
                 in return $
                    DigitalObject
                      (DigitalObjectID $ fromJust $ Solr.id doc)
                      (UserID 0)
                      0
                      (Metadata (SchemaID 0) (DC.toDublinCore doc))
        Nothing -> throwError err400
    req =
      H.setRequestHost "b2find.eudat.eu" $ H.setRequestPath "/solr/select" $
      H.setRequestPort 80 $
      H.setRequestSecure False $
      H.setRequestMethod
        "GET"
      -- $ H.setRequestHeaders [("Authorization", BS.pack (fromMaybe "" auth))]
      -- $ H.setRequestBasicAuth "test" "test"
      -- $ H.setRequestQueryString [("wt", Just "json")]
        H.defaultRequest

extendedEudatAPI
  -- Communities
 =
  getCommunityIDs :<|> (\_ -> throwError err501) :<|> getCommunity :<|>
  (\_ -> throwError err501) :<|>
  (\_ _ _ -> throwError err501) :<|>
  throwError err501 :<|>
  (\_ -> throwError err501) :<|>
  (\_ -> throwError err501) :<|>
  (\_ _ _ -> throwError err501) :<|>
  throwError err501 :<|>
  (\_ -> throwError err501)
  where
    getCommunityIDs :: Handler [CommunityID]
    getCommunityIDs = do
      res <-
        H.httpJSON
          (H.setRequestQueryString
             [("all_fields", Just "true")]
             (req "group_list"))
      return $ map (CommunityID . Ckan.id) (Ckan.result $ H.getResponseBody res)
    getCommunity :: CommunityID -> Handler Community
    getCommunity (CommunityID id') = do
      res <-
        H.httpJSON
          (H.setRequestQueryString
             [("all_fields", Just "true")]
             (req "group_list"))
      let coms =
            filter
              (\x -> Ckan.id x == id')
              (Ckan.result $ H.getResponseBody res)
      if length coms /= 1
        then throwError err404
        else let com = head coms
             in return
                  Community
                  { id = (CommunityID . Ckan.id) com
                  , name = Ckan.title com
                  , desc = Ckan.description com
                  , schemas = []
                  }
    req command =
      H.setRequestHost "b2find.eudat.eu" $
      H.setRequestPath (BS.pack ("/api/3/action/" ++ command)) $
      H.setRequestPort 80 $
      H.setRequestSecure False $
      H.setRequestMethod
        "GET"
      -- $ H.setRequestHeaders [("Authorization", BS.pack (fromMaybe "" auth))]
      -- $ H.setRequestBasicAuth "test" "test"
      -- $ H.setRequestQueryString [("wt", Just "json")]
        H.defaultRequest

main :: IO ()
main = runSettings (setServerName "" $ setPort 8081 defaultSettings) eudatApp
-- main = run 8081 eudatApp
-- main = run 8081 mockEudatApp
