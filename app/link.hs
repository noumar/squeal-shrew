{-# LANGUAGE DataKinds #-} -- Servant
{-# LANGUAGE DeriveGeneric #-} -- Records
{-# LANGUAGE DuplicateRecordFields #-} -- Records
{-# LANGUAGE OverloadedStrings #-} -- HTTP Simple
{-# LANGUAGE TypeOperators #-} -- Servant

module Main where

import qualified Control.Concurrent.STM as T
import qualified Control.Exception.Safe as E
import Control.Monad.IO.Class (liftIO)
import qualified Control.Monad.Reader as R
import Data.Aeson (FromJSON, ToJSON)
import qualified Data.ByteString.Lazy.Char8 as BL
import qualified Data.Time.Clock as C
import GHC.Generics
import qualified Network.HTTP.Simple as H
import Network.Wai.Handler.Warp (run)
import qualified Network.Wai.Middleware.RequestLogger as WL
import Servant

type API = "check" :> ReqBody '[ JSON] LinkRequest :> Post '[ JSON] LinkResponse

type Cache = T.TVar [(H.Request, Checked)]

type ReaderHandler = R.ReaderT Cache Handler

data LinkRequest = LinkRequest
  { url :: String
  , poop :: String
  } deriving (Show, Generic)

data LinkResponse = LinkResponse
  { reason :: String
  , obj :: Checked
  } deriving (Show, Generic)

data Checked = Checked
  { time :: C.UTCTime
  , url :: String
  , code :: Int
  } deriving (Show, Generic)

instance FromJSON LinkRequest

instance ToJSON LinkResponse

instance ToJSON Checked

api :: Proxy API
api = Proxy

reader2Handler :: Cache -> ReaderHandler a -> Handler a
reader2Handler ss r = R.runReaderT r ss

server :: Cache -> Server API
server cache = hoistServer api (reader2Handler cache) serverT

serverT :: ServerT API ReaderHandler
serverT = link

link :: LinkRequest -> ReaderHandler LinkResponse
link body = do
  cache <- R.ask
  x <-
    liftIO . E.tryAny $
     do
      let u = url (body :: LinkRequest)
      req <- H.parseRequest u
      res <- H.httpLBS req
      now <- C.getCurrentTime
      T.atomically $ do
        let n =
              ( req
              , Checked
                {time = now, url = u, code = H.getResponseStatusCode res})
        t <- T.readTVar cache
        T.writeTVar cache (n : t)
        return n
  case x of
    Left e -> throwError err400 {errBody = (BL.pack . show) e}
    Right y -> return LinkResponse {reason = "All good!", obj = snd y}

main :: IO ()
main = do
  cache <- T.atomically (T.newTVar [])
  run 8081 $ WL.logStdoutDev $ serve api (server cache)
