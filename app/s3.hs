{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}
{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

module Main where

import Servant
import qualified Data.ByteString.Lazy.Char8 as BC
import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString as BS
import qualified Test.QuickCheck as QC
import Test.QuickCheck.Instances ()
import qualified Network.Wai.Middleware.RequestLogger as WL
import Control.Monad.IO.Class (liftIO)
import Network.Wai.Handler.Warp (defaultSettings, runSettings, setPort, setServerName, pauseTimeout, setTimeout)
import Data.Text.Encoding (decodeUtf8)
import qualified Data.Map as Map
import Control.Exception (bracket, try)
import Data.Coerce (coerce)
import Control.Monad.Except
import qualified Data.Text as T
import Prelude
import Data.Either (fromRight)

import Irods
import IrodsC

type IrodsPath = T.Text

newtype File =
  File BL.ByteString
  deriving newtype (MimeUnrender OctetStream, MimeRender OctetStream, QC.Arbitrary)

data User = User
  { user :: UserName
  , pass :: Password
  } deriving stock (Eq, Show)

instance Show a => MimeRender PlainText a where
  mimeRender _ = BC.pack . show

instance ToHttpApiData BS.ByteString where
  toHeader x = x

userDB = Map.fromList [ (user u, u) | u <- users ]
  where
    users = []

type API =
  -- Objects
  BasicAuth "irods" User :> "objects" :> CaptureAll "path" IrodsPath
    :> Header' '[Required] "Server" Host
    :> Header' '[Required] "Zone" Zone
    :> StreamGet NoFraming OctetStream (StreamGenerator File)
  :<|>
  BasicAuth "irods" User :> "objects" :> CaptureAll "path" IrodsPath
    :> Header' '[Required] "Server" Host
    :> Header' '[Required] "Zone" Zone
    :> QueryFlag "force"
    :> QueryFlag "checksum"
    :> QueryFlag "pid"
    :> ReqBody '[OctetStream] File
    :> PutNoContent '[JSON] (Headers '[Header "X-PID" BS.ByteString, Header "X-Checksum" BS.ByteString] NoContent)
  :<|>
  BasicAuth "irods" User :> "objects" :> CaptureAll "path" IrodsPath
    :> Header' '[Required] "Server" Host
    :> Header' '[Required] "Zone" Zone
    :> DeleteNoContent '[JSON] NoContent
  -- Collections
  :<|>
  BasicAuth "irods" User :> "collections" :> CaptureAll "path" IrodsPath
    :> Header' '[Required] "Server" Host
    :> Header' '[Required] "Zone" Zone
    :> QueryFlag "recursive"
    :> Get '[JSON] [CollectionEntry]
  :<|>
  BasicAuth "irods" User :> "collections" :> CaptureAll "path" IrodsPath
    :> Header' '[Required] "Server" Host
    :> Header' '[Required] "Zone" Zone
    :> QueryFlag "recursive"
    :> QueryFlag "nothrash"
    :> DeleteNoContent '[JSON] NoContent
  :<|>
  BasicAuth "irods" User :> "collections" :> CaptureAll "path" IrodsPath
    :> Header' '[Required] "Server" Host
    :> Header' '[Required] "Zone" Zone
    :> QueryFlag "recursive"
    :> PutNoContent '[JSON] NoContent

api =
  streamGetFile :<|>
  putObject :<|>
  deleteObject :<|>
  getCollection :<|>
  deleteCollection :<|>
  putCollection
  where
    getObject :: User -> [IrodsPath] -> Host -> Zone -> Handler File
    getObject usr path host zone = do
      let iRodsFile = T.intercalate "/" $ ["", coerce zone, "home", coerce $ user usr] ++ path
      r <- liftIO $ withConnection usr host zone
        (\case
          Left l -> return $ Left l
          Right connection ->
            withObjectOpen connection iRodsFile
              (\case
                Left l -> return $ Left l
                Right object -> Right . File <$> rcDataObjReadLazy connection object
              )
        )
      either throwError return r

    putObject :: User -> [IrodsPath] -> Host -> Zone -> Bool -> Bool -> Bool -> File
      -> Handler (Headers '[Header "X-PID" BS.ByteString, Header "X-Checksum" BS.ByteString] NoContent)
    putObject usr path host zone overwrite checksum pid (File file) = do
      let iRodsFile = T.intercalate "/" $ ["", coerce zone, "home", coerce $ user usr] ++ path
      r <- liftIO $ withConnection usr host zone
        (\case
          Left l -> return $ Left l
          Right connection ->
            withObjectCreate connection iRodsFile getSettings
              (\case
                Left l -> return $ Left l
                Right object -> do
                  print =<< rcDataObjWriteLazy connection object file
                  return $ Right $ NoContent
              )
            >>= getChecksum connection iRodsFile
            >>= getPID connection
        )
      either throwError return r
      where
        getSettings :: KeywordValuePairs
        getSettings = (KeywordValuePairs . map snd . filter fst) [
            -- (True, (destinationResourceName, "disk")),
            (overwrite, (forceFlagKW, ""))
          ]

        getChecksum :: Connection -> IrodsPath
          -> Either ServantErr NoContent
          -> IO (Either ServantErr (Headers '[Header "X-Checksum" BS.ByteString] NoContent))
        getChecksum connection iRodsFile = \case
          Left l -> return $ Left l
          Right _ -> case checksum of
            False -> return $ Right (noHeader NoContent)
            True -> do
              chksum <-
                rcDataObjChksum
                  connection
                  DataObject
                    { objectPath = iRodsFile
                    , createMode = 0
                    , openFlags = readOnly
                    , dataSize = 0
                    , numThreads = 0
                    , conditions = KeywordValuePairs []
                    }
              return $ Right $ newHeader chksum NoContent
          where
            newHeader (Just h) = addHeader h
            newHeader (Nothing) = noHeader

        getPID :: Connection
          -> Either ServantErr (Headers '[Header "X-Checksum" BS.ByteString] NoContent)
          -> IO (Either ServantErr (Headers '[Header "X-PID" BS.ByteString, Header "X-Checksum" BS.ByteString] NoContent))
        getPID connection headers =
          case headers of
            Left l -> return $ Left l
            Right _ -> case pid of
              False -> return $ Right $ emptyHeader
              True -> do
                qr <-
                  rcGenQuery
                    connection
                    QueryInput
                      { maxRows = 1
                      , continueIndex = 0
                      , rowOffset = 0
                      , options = []
                      , conditions = KeywordValuePairs []
                      , select = IndexIntValuePairs [(columnMetadataDataAttributeValue, orderBy)]
                      , sqlConditions = IndexValuePairs [(columnMetadataDataAttributeName, "='PID'")]
                      }
                return $ Right $ case qr of
                  Nothing -> emptyHeader
                  Just r -> newHeader r
          where
            earlierHeaders = fromRight (noHeader NoContent) headers
            emptyHeader = noHeader $ earlierHeaders
            newHeader r = addHeader (snd $ unSqlResult $ head $ sqlResults r) earlierHeaders

    deleteObject :: User -> [IrodsPath] -> Host -> Zone -> Handler NoContent
    deleteObject usr path host zone = do
      let iRodsFile = T.intercalate "/" $ ["", coerce zone, "home", coerce $ user usr] ++ path
      r <- liftIO $ withConnection usr host zone
        (\case
          Left l -> return $ Left l
          Right connection -> do
            unlinkResult <-
              rcDataObjUnlink
                connection
                DataObject
                  { objectPath = iRodsFile
                  , createMode = 0
                  , openFlags = readWrite
                  , dataSize = 0
                  , numThreads = 1
                  , conditions = KeywordValuePairs []
                  }
            return $ case unlinkResult of
              Left FileInvalidPath -> Left err404
              Left FileInternalNullInput -> Left err400
              Left FileOverwriteWithoutForce -> Left err409 {errBody = "Object exists!"}
              Left FileUnhandled -> Left err400
              Right _ -> Right NoContent
        )
      either throwError return r

    getCollection :: User -> [IrodsPath] -> Host -> Zone -> Bool -> Handler [CollectionEntry]
    getCollection usr path host zone recursive = do
      let iRodsFile = T.intercalate "/" $ ["", coerce zone, "home", coerce $ user usr] ++ path
      r <- liftIO $ withConnection usr host zone
        (\case
          Left l -> return $ Left l
          Right connection -> do
            openResult <-
              rcOpenCollection
                connection
                CollectionInput
                  { collectionName = iRodsFile
                  , flags = getSettings
                  , conditions = KeywordValuePairs []
                  }
            case openResult of
              Left CollectionNotFound -> return $ Left err404
              Left CollectionIsObject -> return $ Left err422
              Left _ -> return $ Left err400
              Right collection -> do
                readResult <- rcReadCollection connection collection
                print =<< rcCloseCollection connection collection
                return $ case readResult of
                  Left CollectionEmpty -> Right []
                  Left _ -> Left err400
                  Right coll -> Right coll
        )
      either throwError return r
      where
        getSettings = (map snd . filter fst) [
            (True, veryLongMetadata),
            (recursive, recursiveQuery)
          ]

    deleteCollection :: User -> [IrodsPath] -> Host -> Zone -> Bool -> Bool -> Handler NoContent
    deleteCollection usr path host zone recursive force = do
      let iRodsFile = T.intercalate "/" $ ["", coerce zone, "home", coerce $ user usr] ++ path
      r <- liftIO $ withConnection usr host zone
        (\case
          Left l -> return $ Left l
          Right connection -> do
            rmResult <-
              rcRmColl
                connection
                CollectionInput
                  { collectionName = iRodsFile
                  , flags = []
                  , conditions = getSettings
                  }
            return $ case rmResult of
              CollectionEmpty -> Left err404
              CollectionNotFound -> Left err404
              CollectionNotEmpty -> Left err409 {errBody = "Collection not empty!"}
              CollectionExists -> Left err409 {errBody = "Collection exists!"}
              CollectionIsObject -> Left err422
              CollectionUnhandled -> Left err400
              CollectionOK -> Right NoContent
        )
      either throwError return r
      where
        getSettings :: KeywordValuePairs
        getSettings = (KeywordValuePairs . map snd . filter fst) [
            (recursive, (recursiveOperationKW, "")),
            (force, (forceFlagKW, ""))
          ]

    putCollection :: User -> [IrodsPath] -> Host -> Zone -> Bool -> Handler NoContent
    putCollection usr path host zone recursive = do
      let iRodsFile = T.intercalate "/" $ ["", coerce zone, "home", coerce $ user usr] ++ path
      r <- liftIO $ withConnection usr host zone
        (\case
          Left l -> return $ Left l
          Right connection -> do
            createResult <-
              rcCollCreate
                connection
                CollectionInput
                  { collectionName = iRodsFile
                  , flags = []
                  , conditions = getSettings
                  }
            return $ case createResult of
              CollectionEmpty -> Left err404
              CollectionNotFound -> Left err404
              CollectionNotEmpty -> Left err409 {errBody = "Collection not empty!"}
              CollectionExists -> Left err409 {errBody = "Collection exists!"}
              CollectionIsObject -> Left err422
              CollectionUnhandled -> Left err400
              CollectionOK -> Right NoContent
        )
      either throwError return r
      where
        getSettings :: KeywordValuePairs
        getSettings = (KeywordValuePairs . map snd . filter fst) [
            (recursive, (recursiveOperationKW, ""))
          ]

    withConnection :: User -> Host -> Zone
      -> (Either ServantErr Connection -> IO (Either ServantErr a))
      -> IO (Either ServantErr a)
    withConnection usr host zone = bracket
      (do
        connResult <-
          rcConnect
            host
            (Port 1247)
            (user usr)
            zone
            (Flags 0)
        case connResult of
          Left _ -> return $ Left err504
          Right connection -> do
            loginResult <- clientLoginWithPassword connection (pass usr)
            case loginResult of
              ConnectionOK -> return $ Right connection
              _ -> return $ Left err401
      )
      (either (const $ return 0) rcDisconnect)

    withObjectOpen :: Connection -> IrodsPath
      -> (Either ServantErr FileDescriptor -> IO (Either ServantErr a))
      -> IO (Either ServantErr a)
    withObjectOpen connection file = bracket
      (do
        openResult <-
          rcDataObjOpen
            connection
            DataObject
              { objectPath = file
              , createMode = 0
              , openFlags = readOnly
              , dataSize = 0
              , numThreads = 16
              , conditions = KeywordValuePairs []
              }
        return $ case openResult of
          Left FileInvalidPath -> Left err404
          Left FileInternalNullInput -> Left err400
          Left FileOverwriteWithoutForce -> Left err409 {errBody = "Object exists!"}
          Left FileUnhandled -> Left err400
          Right object -> Right object
      )
      (either (const $ return 0) (rcDataObjClose connection))

    withObjectCreate :: Connection -> IrodsPath -> KeywordValuePairs
      -> (Either ServantErr FileDescriptor -> IO (Either ServantErr a))
      -> IO (Either ServantErr a)
    withObjectCreate connection file keyvalpairs = bracket
      (do
        createResult <-
          rcDataObjCreate
            connection
            DataObject
              { objectPath = file
              , createMode = 0o655
              , openFlags = readWrite
              , dataSize = 0
              , numThreads = 16
              , conditions = keyvalpairs
              }
        return $ case createResult of
          Left FileInvalidPath -> Left err404
          Left FileInternalNullInput -> Left err400
          Left FileOverwriteWithoutForce -> Left err409 {errBody = "Object exists!"}
          Left FileUnhandled -> Left err400
          Right object -> Right object
      )
      (either (const $ return 0) (rcDataObjClose connection))

    streamGetFile :: User -> [IrodsPath] -> Host -> Zone -> Handler (StreamGenerator File)
    streamGetFile usr path host zone = do
      let iRodsFile = T.intercalate "/" $ ["", coerce zone, "home", coerce $ user usr] ++ path
      connResult <- liftIO $
        rcConnect
          host
          (Port 1247)
          (user usr)
          zone
          (Flags 0)
      case connResult of
        Left _ -> throwError err504
        Right connection -> do
          loginResult <- liftIO $ clientLoginWithPassword connection (pass usr)
          case loginResult of
            ConnectionOK -> do
              openResult <- liftIO $
                rcDataObjOpen
                  connection
                  DataObject
                    { objectPath = iRodsFile
                    , createMode = 0
                    , openFlags = readOnly
                    , dataSize = 0
                    , numThreads = 16
                    , conditions = KeywordValuePairs []
                    }
              case openResult of
                Left FileInvalidPath -> throwError err404
                Left FileInternalNullInput -> throwError err400
                Left _ -> throwError err400
                Right (FileDescriptor fd) -> return $
                  StreamGenerator (generator connection
                    OpenObject
                      { l1descInx = fd
                      , len = 0
                      , whence = seekSet
                      , conditions = KeywordValuePairs []
                      })
            _ -> throwError err401
      where
        generator con obj send _ = loop
          where
            loop = do
              chunk <- rcDataObjReadChunk con obj
              if BS.null chunk
              then do
                rcDataObjClose con (FileDescriptor $ l1descInx obj)
                rcDisconnect con
                return ()
              else do
                send $ File $ BL.fromStrict chunk
                loop

eudatAPI :: Proxy API
eudatAPI = Proxy

eudatApp :: Map.Map UserName User -> Application
eudatApp db = WL.logStdoutDev $ serveWithContext eudatAPI ctx eudatServer
  where
    ctx = checkBasicAuth db :. EmptyContext

eudatServer :: Server API
eudatServer = api

checkBasicAuth :: Map.Map UserName User -> BasicAuthCheck User
checkBasicAuth db = BasicAuthCheck $ \basicAuthData ->
  let username = decodeUtf8 (basicAuthUsername basicAuthData)
      password = decodeUtf8 (basicAuthPassword basicAuthData)
  in
  return $ Authorized $ User (UserName username) (Password password)

main :: IO ()
main = runSettings (setServerName "" $ setPort 8082 defaultSettings) (eudatApp userDB)
