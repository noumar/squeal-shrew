{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

module Main where

import qualified Cql
import qualified DublinCore as DC
import qualified Solr

import Control.Lens ((&), (.~), (?~))
import Control.Monad (when)
import qualified Data.ByteString.Char8 as BS
import qualified Data.ByteString.Lazy.Char8 as BL
import Data.Maybe (fromMaybe)
import qualified Data.Swagger as SW
import qualified Data.Text
import Lucid hiding (map_, source_, subject_, title_, type_)
import Lucid.Base
       (makeAttribute, makeElement, makeElementNoEnd, makeXmlElementNoEnd)
import qualified Network.HTTP.Media as M
import qualified Network.HTTP.Simple as H
import Network.Wai.Handler.Warp
       (defaultSettings, runSettings, setPort, setServerName)
import qualified Network.Wai.Middleware.RequestLogger as WL
import Servant
import qualified Servant.Mock as SM
import qualified Servant.Swagger as SSW
import qualified Servant.Swagger.UI as SSWU
import qualified Text.Parsec as P

instance SW.ToParamSchema Cql.CqlQuery where
  toParamSchema = SW.toParamSchema

instance FromHttpApiData Cql.CqlQuery where
  parseQueryParam c =
    case P.parse Cql.cql "(Servant)" (Data.Text.unpack c) of
      Left x -> (Left . Data.Text.pack . show) x
      Right x -> Right x

data XML =
  XML

instance Accept XML where
  contentType _ = "application" M.// "xml" -- M./: ("charset", "utf-8")

instance ToHtml a => MimeRender XML a where
  mimeRender _ = renderBS . toHtml

type API = SRU

-- type API = SSWU.SwaggerSchemaUI "swagger-ui" "swagger.json" :<|> BaseAPI :> SRU
-- type BaseAPI = "sru"
type SRU
   = "sru" -- searchRetrieve
      :> QueryParam "operation" String -- Mandatory
      :> QueryParam "version" Float -- Mandatory
      :> QueryParam "query" String -- Mandatory
      :> QueryParam "startRecord" Int -- Optional
      :> QueryParam "maximumRecords" Int -- Optional
      -- :> QueryParam "recordPacking" String -- Optional
      -- :> QueryParam "recordSchema" String -- Optional
      -- :> QueryParam "resultSetTTL" String -- Optional
      -- :> QueryParam "stylesheet" String -- Optional
      -- :> QueryParam "extraRequestData" String -- Optional
      :> Get '[ XML] (Html ())

sruAPI :: Proxy SRU
sruAPI = Proxy

sruSwaggerAPI :: Proxy API
sruSwaggerAPI = Proxy

sruApp :: Application
sruApp = WL.logStdoutDev $ serve sruAPI sruServer

sruServer :: Server API
sruServer = sru

main :: IO ()
-- main = run 8081 mockSruApp
-- main = run 8081 sruApp
main = runSettings (setServerName "" $ setPort 8081 defaultSettings) sruApp

sru (Just "explain") Nothing Nothing Nothing Nothing = explain
sru (Just "searchRetrieve") (Just v) (Just q) s m =
  searchRetrieve
    v -- version
    q -- query
    (fromMaybe 1 s) -- start, base 1, default 1
    (fromMaybe 10 m) -- max, base 1, default 10
sru Nothing _ _ _ _ = throwError err400 {errBody = "Operation missing!"}
sru _ Nothing _ _ _ = throwError err400 {errBody = "Version missing!"}
sru _ _ Nothing _ _ = throwError err400 {errBody = "Query missing!"}
sru _ _ _ _ _ = throwError err400 {errBody = "Invalid request!"}

searchRetrieve :: Float -> String -> Int -> Int -> Handler (Html ())
searchRetrieve v q s m =
  case P.parse Cql.cql q q of
    Left err ->
      throwError err400 {errBody = BL.pack ("Error parsing " ++ show err)}
    Right p ->
      case Cql.toSolrEither p of
        Left sqErr ->
          throwError
            err400 {errBody = BL.pack $ sqErr ++ " in " ++ "'" ++ show p ++ "'"}
        Right sq -> do
          res <-
            H.httpJSON
              (H.setRequestQueryString
                 [ ("wt", Just "json")
                 , ("echoParams", Just "none")
                 , ("q", Just "*:*")
                 , ("fq", Just $ BS.pack sq)
                 , ("start", (Just . BS.pack . show) $ s - 1) -- offset, base 0
                 , ("rows", (Just . BS.pack . show) m) -- pagination, base 1
                 ]
                 req)
          case Solr.response (H.getResponseBody res) of
            Just sr ->
              return $ do
                let docs = Solr.docs sr
                    found = Solr.numFound sr
                    gotten = length docs
                    next =
                      if gotten > 0
                        then s + gotten
                        else succ s
                xml_
                searchRetrieveResponse_ $ do
                  (versionE_ . toHtml . show) v
                  (numberOfRecords_ . toHtml . show) found
                  records_ $
                    mapM_
                      (\(doc, idx) ->
                         record_ $ do
                           recordSchema_ "dc"
                           recordPacking_ "xml"
                           recordData_ $ (toXml . DC.toDublinCore) doc
                           (recordPosition_ . toHtml . show) idx)
                      (zip docs [s ..])
                  when (next <= found) $
                    (nextRecordPosition_ . toHtml . show) next
                  echoedSearchRetrieveRequest_ $ do
                    (versionE_ . toHtml . show) v
                    (query_ . toHtml) q
                    with
                      (makeElement "sru:xQuery")
                      [ makeAttribute
                          "xmlns:xcql"
                          "http://www.loc.gov/zing/cql/xcql/"
                      ] $
                      makeElement "xcql:searchClause" $ do
                        makeElement "xcql:index" "nil"
                        makeElement "xcql:relation" $
                          makeElement "xcql:value" "nil"
                        makeElement "xcql:term" "nil"
                    (maximumRecords_ . toHtml . show) m
                    recordPacking_ "xml"
                    recordSchema_ "dc"
                  extraResponseData_ $ (solrQuery_ . toHtml) sq
            Nothing -> throwError err400
  where
    req =
      H.setRequestHost "b2find.eudat.eu" $
      H.setRequestPath "/solr/select" $
      H.setRequestPort 80 $
      H.setRequestSecure False $ H.setRequestMethod "GET" H.defaultRequest
      -- $ H.setRequestHeaders [("Authorization", BS.pack (fromMaybe "" auth))]
      -- $ H.setRequestBasicAuth "test" "test"
    -- Response Parameters
    -- diagnostics_ = makeElement "diagnostics"
    echoedSearchRetrieveRequest_ = makeElement "sru:echoedSearchRetrieveRequest"
    extraResponseData_ = makeElement "sru:extraResponseData"
    nextRecordPosition_ = makeElement "sru:nextRecordPosition"
    numberOfRecords_ = makeElement "sru:numberOfRecords"
    records_ = makeElement "sru:records"
    -- resultSetId_ = makeElement "sru:resultSetId"
    -- resultSetIdleTime_ = makeElement "sru:resultSetIdleTime"
    searchRetrieveResponse_ =
      with
        (makeElement "sru:searchRetrieveResponse")
        [ makeAttribute "xmlns:sru" "http://www.loc.gov/zing/srw/"
        , makeAttribute "xmlns:xsi" "http://www.w3.org/2001/XMLSchema-instance"
        , makeAttribute
            "xsi:schemaLocation"
            "http://www.loc.gov/zing/srw/ http://www.loc.gov/standards/sru/xmlFiles/srw-types.xsd"
        ]
    query_ = makeElement "sru:query"
    maximumRecords_ = makeElement "sru:maximumRecords"
    -- Record Parameters
    -- extraRecordData_ = makeElement "sru:extraRecordData"
    -- recordIdentifier_ = makeElement "sru:recordIdentifier"
    solrQuery_ =
      with (makeElement "solr:query") [makeAttribute "xmlns:solr" "Solr"]

recordData_ = makeElement "sru:recordData"

recordPacking_ = makeElement "sru:recordPacking"

recordPosition_ = makeElement "sru:recordPosition"

recordSchema_ = makeElement "sru:recordSchema"

record_ = makeElement "sru:record"

versionE_ = makeElement "sru:version"

xml_ = makeElementNoEnd "?xml version=\"1.0\" encoding=\"utf-8\"?"

class ToXml a where
  toXml :: a -> Html ()

instance ToXml DC.DublinCore where
  toXml dc =
    dc_ $ do
      mapM_ (title_ . toHtml) (DC.title dc)
      mapM_ (creator_ . toHtml) (DC.creator dc)
      mapM_ (subject_ . toHtml) (DC.subject dc)
      mapM_ (description_ . toHtml) (DC.description dc)
      mapM_ (publisher_ . toHtml) (DC.publisher dc)
      mapM_ (contributor_ . toHtml) (DC.contributor dc)
      mapM_ (date_ . toHtml . show) (DC.date dc)
      mapM_ (type_ . toHtml) (DC.type' dc)
      mapM_ (format_ . toHtml) (DC.format dc)
      mapM_ (identifier_ . toHtml) (DC.identifier dc)
      mapM_ (source_ . toHtml) (DC.source dc)
      mapM_ (language_ . toHtml) (DC.language dc)
      mapM_ (relation_ . toHtml) (DC.relation dc)
      mapM_ (coverage_ . toHtml) (DC.coverage dc)
      mapM_ (rights_ . toHtml) (DC.rights dc)
    where
      contributor_ = makeElement "dc:contributor"
      coverage_ = makeElement "dc:coverage"
      creator_ = makeElement "dc:creator"
      date_ = makeElement "dc:date"
      dc_ =
        with
          (makeElement "dc")
          [ makeAttribute "xmlns:dc" "info:srw/schema/1/dc-schema"
          , makeAttribute
              "xmlns:xsi"
              "http://www.w3.org/2001/XMLSchema-instance"
          , makeAttribute
              "xsi:schemaLocation"
              "info:srw/schema/1/dc-schema http://www.loc.gov/standards/sru/recordSchemas/dc-schema.xsd"
          ]
      description_ = makeElement "dc:description"
      format_ = makeElement "dc:format"
      identifier_ = makeElement "dc:identifier"
      language_ = makeElement "dc:language"
      publisher_ = makeElement "dc:publisher"
      relation_ = makeElement "dc:relation"
      rights_ = makeElement "dc:rights"
      source_ = makeElement "dc:source"
      subject_ = makeElement "dc:subject"
      title_ = makeElement "dc:title"
      type_ = makeElement "dc:type"

explain :: Handler (Html ())
explain =
  return $ do
    xml_
    explainResponse_ $ do
      versionE_ "1.2"
      record_ $ do
        recordSchema_ "http://explain.z3950.org/dtd/2.0/"
        recordPacking_ "xml"
        recordData_ $
          explain_ $ do
            serverInfo_
              [ protocol_ "SRU"
              , versionA_ "1.2"
              , transport_ "http"
              --, method_ "GET"
              ] $ do
              host_ "b2find.eudat.eu"
              port_ "80"
              database_ "sru"
            databaseInfo_ $ do
              title_ "B2FIND -- Aggregated EUDAT metadata domain Data inventory"
              description_
                [lang_ "en", primary_ "true"]
                "SRU/Z39.50 Gateway to B2FIND server. Records in UTF-8 encoding."
            indexInfo_ $ do
              setE_
                [ identifier_ "info:srw/cql-context-set/1/cql-v1.2"
                , nameA_ "cql"
                ]
              setE_
                [identifier_ "info:srw/cql-context-set/1/dc-v1.1", nameA_ "dc"]
              setE_
                [ identifier_ "http://zing.z3950.org/cql/bath/2.0"
                , nameA_ "bath"
                ]
              setE_
                [identifier_ "info:srw/cql-context-set/2/rec-1.1", nameA_ "rec"]
              setE_
                [ identifier_ "http://zing.z3950.org/cql/local/1.1"
                , nameA_ "local"
                ]
              index_ [] $ do
                title_ "All Indexes"
                map_ $ nameE_ [setA_ "cql"] "allIndexes"
              index_ [] $ do
                title_ "Title"
                map_ $ nameE_ [setA_ "dc"] "title"
              index_ [] $ do
                title_ "Creator"
                map_ $ nameE_ [setA_ "dc"] "creator"
                map_ $ nameE_ [setA_ "dc"] "author"
              index_ [] $ do
                title_ "Subject"
                map_ $ nameE_ [setA_ "dc"] "subject"
              index_ [] $ do
                title_ "Description"
                map_ $ nameE_ [setA_ "dc"] "description"
              index_ [] $ do
                title_ "Publisher"
                map_ $ nameE_ [setA_ "dc"] "publisher"
              index_ [] $ do
                title_ "Contributor"
                map_ $ nameE_ [setA_ "dc"] "contributor"
              index_ [] $ do
                title_ "Date"
                map_ $ nameE_ [setA_ "dc"] "date"
{-
              index_ [id_ ""] $ do
                title_ "Type"
                map_ $ nameE_ [setA_ "dc"] "type"
-}
              index_ [] $ do
                title_ "Format"
                map_ $ nameE_ [setA_ "dc"] "format"
              index_ [] $ do
                title_ "Identifier"
                map_ $ nameE_ [setA_ "dc"] "identifier"
              index_ [] $ do
                title_ "Source"
                map_ $ nameE_ [setA_ "dc"] "source"
              index_ [] $ do
                title_ "Language"
                map_ $ nameE_ [setA_ "dc"] "language"
{-
              index_ [id_ ""] $ do
                title_ "Relation"
                map_ $ nameE_ [setA_ "dc"] "relation"
              index_ [id_ ""] $ do
                title_ "Coverage"
                map_ $ nameE_ [setA_ "dc"] "coverage"
-}
              index_ [] $ do
                title_ "Rights"
                map_ $ nameE_ [setA_ "dc"] "rights"
              index_ [] $ do
                title_ "Record Created"
                map_ $ nameE_ [setA_ "rec"] "created"
              index_ [] $ do
                title_ "Record Last Modified"
                map_ $ nameE_ [setA_ "rec"] "lastModified"
{-
              index_ [id_ ""] $ do
                title_ "Name"
                map_ $ nameE_ [setA_ "bath"] "name"
              index_ [id_ ""] $ do
                title_ "Personal Name"
                map_ $ nameE_ [setA_ "bath"] "personalName"
              index_ [id_ ""] $ do
                title_ "Corporate Name"
                map_ $ nameE_ [setA_ "bath"] "corporateName"
              index_ [id_ ""] $ do
                title_ "Conference Name"
                map_ $ nameE_ [setA_ "bath"] "conferenceName"
              index_ [id_ ""] $ do
                title_ "Geographic Name"
                map_ $ nameE_ [setA_ "bath"] "geographicName"
              index_ [id_ ""] $ do
                title_ "ISBN"
                map_ $ nameE_ [setA_ "bath"] "isbn"
              index_ [id_ ""] $ do
                title_ "ISSN"
                map_ $ nameE_ [setA_ "bath"] "issn"
              index_ [id_ ""] $ do
                title_ "LC Control Number"
                map_ $ nameE_ [setA_ "bath"] "lccn"
              index_ [id_ ""] $ do
                title_ "Standard Identifier"
                map_ $ nameE_ [setA_ "bath"] "standardIdentifier"
-}
            schemaInfo_ $ do
              schema_
                [ identifier_ "info:srw/schema/1/marcxml-v1.1"
                , sort_ "false"
                , nameA_ "marcxml"
                ]
                (title_ "MARCXML")
              schema_
                [ identifier_ "info:srw/schema/1/dc-v1.1"
                , sort_ "false"
                , nameA_ "dc"
                ]
                (title_ "Dublin Core")
              schema_
                [ identifier_ "info:srw/schema/1/mods-v3.5"
                , sort_ "false"
                , nameA_ "mods"
                ]
                (title_ "MODS v3.5")
        recordPosition_ "1"
      echoedExplainRequest_ $ do
        versionE_ "1.2"
        recordPacking_ "xml"
  where
    explainResponse_ =
      with
        (makeElement "sru:explainResponse")
        [ makeAttribute "xmlns:sru" "http://www.loc.gov/zing/srw/"
        , makeAttribute "xmlns:xsi" "http://www.w3.org/2001/XMLSchema-instance"
        , makeAttribute
            "xsi:schemaLocation"
            "http://www.loc.gov/zing/srw/ http://www.loc.gov/standards/sru/xmlFiles/srw-types.xsd"
        ]
    explain_ =
      with
        (makeElement "zr:explain")
        [ makeAttribute "xmlns:zr" "http://explain.z3950.org/dtd/2.0/"
        , makeAttribute "xmlns:xsi" "http://www.w3.org/2001/XMLSchema-instance"
        , makeAttribute
            "xsi:schemaLocation"
            "http://explain.z3950.org/dtd/2.0/ http://www.loc.gov/standards/sru/recordSchemas/zeerex-2.0.xsd"
        ]
    description_ = with (makeElement "zr:description")
    index_ = with (makeElement "zr:index")
    nameE_ = with (makeElement "zr:name")
    schema_ = with (makeElement "zr:schema")
    serverInfo_ = with (makeElement "zr:serverInfo")
    setE_ = with (makeXmlElementNoEnd "zr:set")
    -- Simple elements
    echoedExplainRequest_ = makeElement "sru:echoedExplainRequest"
    databaseInfo_ = makeElement "zr:databaseInfo"
    database_ = makeElement "zr:database"
    host_ = makeElement "zr:host"
    indexInfo_ = makeElement "zr:indexInfo"
    port_ = makeElement "zr:port"
    schemaInfo_ = makeElement "zr:schemaInfo"
    title_ = makeElement "zr:title"
    map_ = makeElement "zr:map"
    -- Attributes
    identifier_ = makeAttribute "identifier"
    nameA_ = makeAttribute "name"
    primary_ = makeAttribute "primary"
    protocol_ = makeAttribute "protocol"
    setA_ = makeAttribute "set"
    sort_ = makeAttribute "sort"
    transport_ = makeAttribute "transport"
    versionA_ = makeAttribute "version"
