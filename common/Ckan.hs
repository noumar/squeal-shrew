{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}

module Ckan
  ( Group(..)
  , Response(..)
  , Error(..)
  ) where

import Solr ()

import Data.Aeson (FromJSON)
import Data.UUID.Types (UUID)
import GHC.Generics (Generic)

{-# ANN module ("HLint: ignore Use camelCase" :: String) #-}

instance FromJSON (Response [Group])

data Group = Group
  { id :: UUID
  , name :: String
  , title :: String
  , display_name :: String
  , description :: String
  } deriving (Generic, FromJSON)

data Response a = Response
  { help :: String
  , result :: a
  , success :: Maybe Bool
  , error :: Maybe Error
  } deriving (Generic)

data Error = Error
  { message :: String
  , __type :: String
  } deriving (Generic, FromJSON)
