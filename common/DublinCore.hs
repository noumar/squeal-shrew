{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module DublinCore where

import Data.Aeson.TH
import qualified Data.Swagger as SW
import qualified Data.Text as T
import qualified Data.Time.Clock as TC
import GHC.Generics (Generic)
import qualified Test.QuickCheck as QC
import Test.QuickCheck.Instances ()

data DublinCore = DublinCore
  { title :: [String]
  , creator :: [T.Text]
  , subject :: [String]
  , description :: [String]
  , publisher :: [T.Text]
  , contributor :: [T.Text]
  , date :: [TC.UTCTime]
  , type' :: [String]
  , format :: [String]
  , identifier :: [String]
  , source :: [String]
  , language :: [String]
  , relation :: [String]
  , coverage :: [String]
  , rights :: [T.Text]
  } deriving (Generic, Show)

class ToDublinCore a where
  toDublinCore :: a -> DublinCore

$(deriveJSON
    defaultOptions
    { fieldLabelModifier =
        \s ->
          if last s == '\''
            then init s
            else s
    }
    ''DublinCore)

instance SW.ToSchema DublinCore

instance QC.Arbitrary DublinCore where
  arbitrary =
    DublinCore <$> QC.arbitrary <*> QC.arbitrary <*> QC.arbitrary <*>
    QC.arbitrary <*>
    QC.arbitrary <*>
    QC.arbitrary <*>
    QC.arbitrary <*>
    QC.arbitrary <*>
    QC.arbitrary <*>
    QC.arbitrary <*>
    QC.arbitrary <*>
    QC.arbitrary <*>
    QC.arbitrary <*>
    QC.arbitrary <*>
    QC.arbitrary
