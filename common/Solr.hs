{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Solr
  ( SolrResponse(..)
  , SolrHeader(..)
  , SolrResult(..)
  , SolrError(..)
  , SolrDoc(..)
  ) where

import qualified DublinCore as DC

import Data.Aeson (FromJSON(parseJSON), Value(Object), (.:), (.:?))
import Data.Aeson.Types (typeMismatch)
import Data.Maybe (catMaybes, fromMaybe)
import qualified Data.Text as T
import qualified Data.Time.Clock as C
import Data.UUID.Types (UUID)
import GHC.Generics (Generic)

data SolrResponse = SolrResponse
  { responseHeader :: SolrHeader
  , response :: Maybe SolrResult
  , error :: Maybe SolrError
  } deriving (Generic, Show, FromJSON)

data SolrHeader = SolrHeader
  { status :: Int
  , qTime :: Int
  } deriving (Show)

data SolrResult = SolrResult
  { numFound :: Int
  , start :: Int
  , docs :: [SolrDoc]
  } deriving (Generic, Show, FromJSON)

data SolrError = SolrError
  { msg :: String
  , code :: Int
  } deriving (Generic, Show, FromJSON)

data SolrDoc = SolrDoc
  { id :: Maybe UUID -- identifier?
  , title :: Maybe String -- title
  , author :: Maybe T.Text -- creator
  , discipline :: Maybe String -- subject?
  , tags :: Maybe [String] -- subject?
  , notes :: Maybe String -- description
  , publisher :: Maybe T.Text -- publisher
  , contact :: Maybe T.Text -- contributor
  , publicationTimestamp :: Maybe C.UTCTime -- date
  , format :: Maybe String -- format
  , doi :: Maybe String -- identifier?
  , oaiIdentifier :: Maybe String -- identifier?
  , metaDataAccess :: Maybe String -- source
  , language :: Maybe String -- language
  , temporalCoverageBeginDate :: Maybe C.UTCTime -- coverage
  , temporalCoverageEndDate :: Maybe C.UTCTime -- coverage
  , rights :: Maybe T.Text -- rights
  } deriving (Generic, Show)

instance FromJSON SolrHeader where
  parseJSON (Object v) = SolrHeader <$> v .: "status" <*> v .: "QTime"
  parseJSON invalid = typeMismatch "SolrHeader" invalid

instance FromJSON SolrDoc where
  parseJSON (Object v) =
    SolrDoc <$> v .:? "id" <*> v .:? "title" <*> v .:? "author" <*>
    v .:? "extras_Discipline" <*>
    v .:? "tags" <*>
    v .:? "notes" <*>
    v .:? "extras_Publisher" <*>
    v .:? "extras_Contact" <*>
    v .:? "extras_PublicationTimestamp" <*>
    v .:? "extras_Format" <*>
    v .:? "extras_DOI" <*>
    v .:? "extras_oai_identifier" <*>
    v .:? "extras_MetaDataAccess" <*>
    v .:? "extras_Language" <*>
    v .:? "extras_TemporalCoverageBeginDate" <*>
    v .:? "extras_TemporalCoverageEndDate" <*>
    v .:? "extras_Rights"
  parseJSON invalid = typeMismatch "SolrDoc" invalid

instance DC.ToDublinCore SolrDoc where
  toDublinCore doc =
    DC.DublinCore
    { DC.title = catMaybes [title doc]
    , DC.creator = maybe [] (map T.strip . T.splitOn ";") (author doc)
    , DC.subject = catMaybes [discipline doc] ++ fromMaybe [] (tags doc)
    , DC.description = catMaybes [notes doc]
    , DC.publisher = maybe [] (map T.strip . T.splitOn ";") (publisher doc)
    , DC.contributor = maybe [] (map T.strip . T.splitOn ";") (contact doc)
    , DC.date = catMaybes [publicationTimestamp doc >>= Just]
    , DC.type' = []
    , DC.format = catMaybes [format doc]
    , DC.identifier = catMaybes [doi doc, oaiIdentifier doc]
    , DC.source = catMaybes [metaDataAccess doc]
    , DC.language = catMaybes [language doc]
    , DC.relation = []
    , DC.coverage =
        catMaybes
          [ temporalCoverageBeginDate doc >>= (Just . show)
          , temporalCoverageEndDate doc >>= (Just . show)
          ]
    , DC.rights = maybe [] (map T.strip . T.splitOn ";") (rights doc)
    }
