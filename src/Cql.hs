{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE Safe #-}

module Cql
  ( cql
  , cql11
  , cql12
  , CqlQuery
  , toSolr
  , toSolrEither
  ) where

import Control.Monad.Error
import qualified Data.Char as C
import Data.Functor.Identity
import qualified Data.List as L
import Text.Parsec ((<|>))
import qualified Text.Parsec as P

toSolrEither :: CqlQuery -> Either String String
toSolrEither q = (runIdentity . runErrorT) (toSolr q)

data CqlQuery
  = SortedQuery (Maybe CqlPrefixAssignment)
                CqlQuery
                (Maybe CqlSortSpec)
  | ScopedClause (Maybe CqlQuery)
                 (Maybe CqlBooleanGroup)
                 CqlQuery
  | SearchClause (Maybe CqlIndex)
                 (Maybe CqlRelation)
                 CqlQuery
  | SearchTerm String
  deriving (Show, Eq)

newtype CqlUri =
  Uri String
  deriving (Show, Eq)

newtype CqlIndex =
  Index String
  deriving (Show, Eq)

newtype CqlPrefix =
  Prefix String
  deriving (Show, Eq)

data CqlComparitor
  = ComparitorSymbol String
  | NamedComparitor String
  deriving (Show, Eq)

newtype CqlModifierValue =
  ModifierValue String
  deriving (Show, Eq)

newtype CqlModifierName =
  ModifierName String
  deriving (Show, Eq)

newtype CqlBoolean =
  Boolean String
  deriving (Show, Eq)

newtype CqlModifierList =
  ModifierList [CqlModifier]
  deriving (Show, Eq)

data CqlRelation =
  Relation CqlComparitor
           (Maybe CqlModifierList)
  deriving (Show, Eq)

data CqlBooleanGroup =
  BooleanGroup CqlBoolean
               (Maybe CqlModifierList)
  deriving (Show, Eq)

data CqlPrefixAssignment =
  PrefixAssignment (Maybe CqlPrefix)
                   CqlUri
  deriving (Show, Eq)

data CqlModifier =
  Modifier CqlModifierName
           (Maybe CqlComparitor)
           (Maybe CqlModifierValue)
  deriving (Show, Eq)

data CqlSingleSpec =
  SingleSpec CqlIndex
             (Maybe CqlModifierList)
  deriving (Show, Eq)

newtype CqlSortSpec =
  SortSpec [CqlSingleSpec]
  deriving (Show, Eq)

cql :: P.Parsec String () CqlQuery
cql = cql12

-- | CQL 1.2
cql12 :: P.Parsec String () CqlQuery
cql12 = sortedQuery <* P.eof

-- | sortedQuery ::= prefixAssignment sortedQuery | scopedClause ['sortby' sortSpec]
sortedQuery :: P.Parsec String () CqlQuery
sortedQuery =
  do pa <- prefixAssignment
     P.spaces
     sq <- sortedQuery
     return (SortedQuery (Just pa) sq Nothing)
     <|> do
    sc <- scopedClause
    ss <-
      P.optionMaybe
        (do P.spaces
            _ <- string' "sortby"
            P.spaces
            sortSpec)
    return (SortedQuery Nothing sc ss)

-- | sortSpec ::= sortSpec singleSpec | singleSpec
sortSpec :: P.Parsec String () CqlSortSpec
sortSpec = do
  s <- P.many1 (singleSpec <* P.spaces)
  return (SortSpec s)

-- | singleSpec ::= index [modifierList]
singleSpec :: P.Parsec String () CqlSingleSpec
singleSpec = do
  i <- index
  ml <- P.optionMaybe modifierList
  return (SingleSpec i ml)

-- | CQL 1.1
cql11 :: P.Parsec String () CqlQuery
cql11 = cqlQuery <* P.eof

-- | cqlQuery ::= prefixAssignment cqlQuery | scopedClause
cqlQuery :: P.Parsec String () CqlQuery
cqlQuery =
  P.try
    (do pa <- prefixAssignment
        P.spaces
        cq <- cqlQuery
        return (SortedQuery (Just pa) cq Nothing)) <|> do
    sc <- scopedClause
    return (SortedQuery Nothing sc Nothing)

-- | prefixAssignment ::= '>' prefix '=' uri | '>' uri
prefixAssignment :: P.Parsec String () CqlPrefixAssignment
prefixAssignment =
  P.try
    (do _ <- P.char '>'
        P.spaces
        p <- prefix
        P.spaces
        _ <- P.char '='
        P.spaces
        u <- uri
        return (PrefixAssignment (Just p) u)) <|> do
    _ <- P.char '>'
    P.spaces
    u <- uri
    return (PrefixAssignment Nothing u)

-- | scopedClause ::= scopedClause booleanGroup searchClause | searchClause
scopedClause :: P.Parsec String () CqlQuery
scopedClause =
  P.try
    (P.chainl1
       searchClause
       (do P.spaces
           bg <- booleanGroup
           P.spaces
           return (\x y -> (ScopedClause (Just x) (Just bg) y)))) <|> do
    se <- searchClause
    return (ScopedClause Nothing Nothing se)

-- | booleanGroup ::= boolean [modifierList]
booleanGroup :: P.Parsec String () CqlBooleanGroup
booleanGroup = do
  b <- boolean
  P.spaces
  ml <- P.optionMaybe modifierList
  return (BooleanGroup b ml)

-- | boolean ::= 'and' | 'or' | 'not' | 'prox'
boolean :: P.Parsec String () CqlBoolean
boolean = do
  b <- string' "and" <|> string' "or" <|> string' "not" <|> string' "prox"
  return (Boolean (map C.toLower b))

-- | searchClause ::= '(' cqlQuery ')' | index relation searchTerm | searchTerm
searchClause :: P.Parsec String () CqlQuery
searchClause =
  do _ <- P.char '('
     P.spaces
     cq <- cqlQuery
     P.spaces
     _ <- P.char ')'
     return (SearchClause Nothing Nothing cq)
     <|> P.try
    (do i <- index
        P.spaces
        r <- relation
        P.spaces
        st <- searchTerm
        return (SearchClause (Just i) (Just r) st)) <|> do
    st <- searchTerm
    return (SearchClause Nothing Nothing st)

-- | relation ::= comparitor [modifierList]
relation :: P.Parsec String () CqlRelation
relation = do
  co <- comparitor
  ml <- P.optionMaybe modifierList
  return (Relation co ml)

-- | comparitor ::= comparitorSymbol | namedComparitor
comparitor :: P.Parsec String () CqlComparitor
-- comparitor = P.try comparitorSymbol <|> namedComparitor
comparitor = comparitorSymbol <|> namedComparitor

-- | comparitorSymbol ::= '=' | '>' | '<' | '>=' | '<=' | '<>' | '=='
comparitorSymbol :: P.Parsec String () CqlComparitor
comparitorSymbol = do
  s <-
    P.try (P.string ">=") <|> P.try (P.string ">") <|> P.try (P.string "<=") <|>
    P.try (P.string "<>") <|>
    P.try (P.string "<") <|>
    P.try (P.string "==") <|>
    P.string "="
  return (ComparitorSymbol s)

-- | namedComparitor ::= identifier
namedComparitor :: P.Parsec String () CqlComparitor
namedComparitor = do
  P.notFollowedBy boolean
  P.notFollowedBy (string' "sortby")
  i <- identifier
  return (NamedComparitor (map C.toLower i))

-- | modifierList ::= modifierList modifier | modifier
modifierList :: P.Parsec String () CqlModifierList
modifierList = do
  m <- P.many1 (modifier <* P.spaces)
  return (ModifierList m)

-- | modifier ::= '/' modifierName [comparitorSymbol modifierValue]
modifier :: P.Parsec String () CqlModifier
modifier = do
  _ <- P.char '/'
  P.spaces
  mn <- modifierName
  cs <- P.optionMaybe comparitorSymbol
  mv <- P.optionMaybe modifierValue
  return (Modifier mn cs mv)

-- | prefix ::= term
prefix :: P.Parsec String () CqlPrefix
prefix = do
  t <- term
  return (Prefix t)

-- | uri ::= term
uri :: P.Parsec String () CqlUri
uri = do
  t <- term
  return (Uri t)

-- | modifierName ::= term
modifierName :: P.Parsec String () CqlModifierName
modifierName = do
  t <- term
  return (ModifierName t)

-- | modifierValue ::= term
modifierValue :: P.Parsec String () CqlModifierValue
modifierValue = do
  t <- term
  return (ModifierValue t)

-- | searchTerm ::= term
searchTerm :: P.Parsec String () CqlQuery
searchTerm = do
  t <- term
  return (SearchTerm t)

-- | index ::= term
index :: P.Parsec String () CqlIndex
index = do
  t <- term
  return (Index t)

-- | term ::= identifier | 'and' | 'or' | 'not' | 'prox' | 'sortby'
term :: P.Parsec String () String
term =
  P.try
    (string' "and" <|> string' "or" <|> string' "not" <|> string' "prox" <|>
     string' "sortby") <|>
  identifier

-- | identifier ::= charString1 | charString2
identifier :: P.Parsec String () String
identifier = charString2 <|> charString1

-- | charString1 ::= Too long, see specs
charString1 :: P.Parsec String () String
charString1 = P.many1 (P.noneOf " ()=<>\"/")

-- | charString2 ::= Too long, see specs
charString2 :: P.Parsec String () String
charString2 = P.between (P.char '"') (P.char '"') (P.many (P.noneOf "\""))

--
-- | Case insensitive version of 'P.string'
string' :: String -> P.Parsec String () String
string' s = P.try (mapM char' s)

-- | Case insensitive version of 'P.char'
char' :: Char -> P.Parsec String () Char
char' c = P.char (C.toLower c) <|> P.char (C.toUpper c)

wrap :: String -> String
wrap s = concat ["(", s, ")"]

toSolr :: (Monad m) => CqlQuery -> m String
toSolr query =
  case query of
    SortedQuery Nothing q Nothing -> wrap <$> toSolr q
    -- SortedQuery (Just p) q Nothing -> wrap (show p ++ toSolr q)
    SortedQuery Nothing q (Just s) ->
      toSolr q >>= \x -> sortToSolr s >>= \y -> return $ wrap x ++ "&sort=" ++ y
    --
    ScopedClause Nothing Nothing q -> toSolr q
    ScopedClause (Just q1) (Just b) q2 ->
      toSolr q1 >>= \x ->
        booleanGroupToSolr b >>= \y ->
          toSolr q2 >>= \z -> return $ unwords [x, y, z]
    --
    SearchTerm t
      | not (all C.isAlphaNum t) -> return $ concat ["\"", t, "\""]
      | otherwise -> return t
    -- Multiple indexes
    SearchClause (Just (indexToSolr -> Just "identifier")) (Just r) q ->
      toSolr
        (SortedQuery
           Nothing
           (ScopedClause
              (Just
                 (SearchClause (Just (Index "solr.oai_identifier")) (Just r) q))
              (Just (BooleanGroup (Boolean "or") Nothing))
              (SearchClause (Just (Index "solr.doi")) (Just r) q))
           Nothing)
    SearchClause (Just (indexToSolr -> Just "subject")) (Just r) q ->
      toSolr
        (SortedQuery
           Nothing
           (ScopedClause
              (Just (SearchClause (Just (Index "solr.discipline")) (Just r) q))
              (Just (BooleanGroup (Boolean "or") Nothing))
              (SearchClause (Just (Index "solr.tags")) (Just r) q))
           Nothing)
    -- Comparitors
    SearchClause (Just i) (Just (Relation (ComparitorSymbol "<") Nothing)) (SearchTerm t) ->
      (++ ":[* TO " ++ t ++ "}") <$> indexToSolr i
    SearchClause (Just i) (Just (Relation (ComparitorSymbol "<=") Nothing)) (SearchTerm t) ->
      (++ ":[* TO " ++ t ++ "]") <$> indexToSolr i
    SearchClause (Just i) (Just (Relation (ComparitorSymbol ">") Nothing)) (SearchTerm t) ->
      (++ ":{" ++ t ++ " TO *]") <$> indexToSolr i
    SearchClause (Just i) (Just (Relation (ComparitorSymbol ">=") Nothing)) (SearchTerm t) ->
      (++ ":[" ++ t ++ " TO *]") <$> indexToSolr i
    SearchClause (Just i) (Just (Relation (ComparitorSymbol "<>") Nothing)) t ->
      ("-" ++) <$>
      toSolr
        (SearchClause
           (Just i)
           (Just (Relation (ComparitorSymbol "=") Nothing))
           t)
    SearchClause (Just i) (Just (Relation (NamedComparitor "within") Nothing)) (SearchTerm t)
      | length (words t) == 2 ->
        let (s:e:_) = words t
        in (++ ":[" ++ s ++ " TO " ++ e ++ "]") <$> indexToSolr i
      | otherwise -> fail (t ++ ": Wrong amount of values!")
    SearchClause (Just (Index i)) (Just (Relation (NamedComparitor "encloses") Nothing)) (SearchTerm t)
      | length (words t) == 1 ->
        let (s:_) = words t
        in toSolr
             (SortedQuery
                Nothing
                (ScopedClause
                   (Just
                      (SearchClause
                         (Just (Index (i ++ "Start")))
                         (Just (Relation (ComparitorSymbol "<=") Nothing))
                         (SearchTerm s)))
                   (Just (BooleanGroup (Boolean "and") Nothing))
                   (SearchClause
                      (Just (Index (i ++ "End")))
                      (Just (Relation (ComparitorSymbol ">=") Nothing))
                      (SearchTerm s)))
                Nothing)
      | otherwise -> fail (t ++ ": Wrong amount of values!")
    SearchClause (Just i) (Just (Relation (NamedComparitor "any") Nothing)) (SearchTerm t) ->
      (++ ":(" ++ unwords (L.intersperse "OR" (words t)) ++ ")") <$>
      indexToSolr i
    SearchClause (Just i) (Just (Relation (NamedComparitor "all") Nothing)) (SearchTerm t) ->
      (++ ":(" ++ unwords (L.intersperse "AND" (words t)) ++ ")") <$>
      indexToSolr i
    --
    SearchClause Nothing Nothing q -> toSolr q
    SearchClause Nothing (Just r) q ->
      relationToSolr r >>= \x -> toSolr q >>= \y -> return $ x ++ y
    SearchClause (Just i) Nothing q ->
      indexToSolr i >>= \x -> toSolr q >>= \y -> return $ x ++ y
    SearchClause (Just i) (Just r) q ->
      indexToSolr i >>= \x ->
        relationToSolr r >>= \y -> toSolr q >>= \z -> return $ x ++ y ++ z
    n -> fail $ "Unexpected node! " ++ show n
  where
    relationToSolr :: (Monad m) => CqlRelation -> m String
    relationToSolr r =
      case r of
        Relation c Nothing -> comparitorToSolr c
        _ -> fail "Relation with modifier(s) can't be converted!"
    comparitorToSolr :: (Monad m) => CqlComparitor -> m String
    comparitorToSolr (ComparitorSymbol s)
      | s == "=" = return ":"
      | otherwise = fail $ "Comparitor symbol " ++ s ++ " can't be converted!"
    comparitorToSolr (NamedComparitor n) =
      fail $ "Named comparitor " ++ n ++ " can't be converted!"
    booleanGroupToSolr :: (Monad m) => CqlBooleanGroup -> m String
    booleanGroupToSolr bg =
      case bg of
        BooleanGroup b Nothing -> booleanToSolr b
        _ -> fail "Boolean group with modifier(s) can't be converted!"
    booleanToSolr :: (Monad m) => CqlBoolean -> m String
    booleanToSolr (Boolean s) =
      case s of
        "and" -> return "AND"
        "not" -> return "NOT"
        "or" -> return "OR"
        _ -> fail $ "Unexpected boolean! " ++ s
    indexToSolr :: (Monad m) => CqlIndex -> m String
    indexToSolr (Index i) =
      case map C.toLower i
          -- DC
            of
        "dc.title" -> return "title"
        "dc.creator" -> return "author"
        "dc.author" -> return "author" -- not really DC
        "dc.subject" -> return "subject"
        "dc.description" -> return "notes"
        "dc.publisher" -> return "extras_Publisher"
        "dc.contributor" -> return "extras_Contact"
        "dc.date" -> return "extras_PublicationTimestamp"
        -- type
        "dc.format" -> return "extras_Format"
        "dc.identifier" -> return "identifier"
        "dc.source" -> return "extras_MetaDataAccess"
        "dc.language" -> return "extras_Language"
        -- relation
        -- coverage
        "dc.rights" -> return "extras_Rights"
          -- REC
        "rec.created" -> return "metadata_created"
        "rec.lastmodified" -> return "metadata_modified"
          -- CQL
        "cql.anyindexes" -> return ""
        "cql.allindexes" -> return "*"
        "cql.allrecords" -> return "baz"
        "cql.anywhere" -> indexToSolr (Index "cql.allIndexes")
        "cql.keywords" -> return "foo"
        "cql.resultsetid" -> return "fube"
        "cql.serverchoice" -> indexToSolr (Index "cql.anyIndexes")
        "anyindexes" -> indexToSolr (Index "cql.anyIndexes")
        "allindexes" -> indexToSolr (Index "cql.allIndexes")
        "allrecords" -> indexToSolr (Index "cql.allRecords")
        "anywhere" -> indexToSolr (Index "cql.anywhere")
        "keywords" -> indexToSolr (Index "cql.keywords")
        "resultsetid" -> indexToSolr (Index "cql.resultSetId")
        "serverchoice" -> indexToSolr (Index "cql.serverChoice")
          --
        "publicationyear" -> return "extras_PublicationYear"
        "datestart" -> return "extras_TemporalCoverageBeginDate"
        "dateend" -> return "extras_TemporalCoverageEndDate"
          -- Solr
        "solr.discipline" -> return "extras_Discipline"
        "solr.doi" -> return "extras_DOI"
        "solr.oai_identifier" -> return "extras_oai_identifier"
        "solr.tags" -> return "tags"
          --
        y
          | not ("dc." `L.isPrefixOf` y) -> indexToSolr (Index ("dc." ++ y))
          | otherwise -> fail $ y ++ ": Can't be converted!"
    sortToSolr :: (Monad m) => CqlSortSpec -> m String
    sortToSolr (SortSpec []) = return []
    sortToSolr (SortSpec (SingleSpec i m:r)) =
      indexToSolr i >>= \x ->
        modToSolr m >>= \y ->
          sortToSolr (SortSpec r) >>= \z -> return $ concat [x, " ", y, ",", z]
      where
        modToSolr :: (Monad m) => Maybe CqlModifierList -> m String
        modToSolr Nothing = return "desc"
        modToSolr (Just (ModifierList (Modifier (ModifierName n) _ _:_))) =
          case n of
            "sort.ascending" -> return "asc"
            "sort.descending" -> return "desc"
            nm -> fail $ "Unexpected modifier! " ++ nm
        modToSolr (Just (ModifierList [])) =
          fail "Unexpected empty modifierlist!"
