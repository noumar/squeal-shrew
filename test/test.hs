{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

import Cql
import Solr

import qualified Data.ByteString.Char8 as S8
import qualified Data.List.Split as LS
import qualified Network.HTTP.Simple as H
import Test.Hspec
import qualified Text.Parsec as P

solrRequest u = do
  req <- H.parseRequest u
  res <- H.httpLBS req -- (H.setRequestIgnoreStatus req)
  return (H.getResponseStatusCode res)

solrRequestJSON u = do
  req <- H.parseRequest u --"http://136.172.11.43/solr/select?wt=json&rows=1000&q=*:*"
  res <- H.httpJSONEither (H.setRequestIgnoreStatus req)
  case H.getResponseBody res of
    Left _ -> fail "JSON parse error"
    Right j -> return ((status . responseHeader) (j :: SolrResponse))

main :: IO ()
main =
  hspec $ do
    describe "CQL" $ do
      it "Empty" $ pendingWith "Fix parser" -- cqlToSolr "" `shouldReturn` "()"
      it "Unquoted space" $ pendingWith "Fix parser" -- cqlToSolr " " `shouldReturn` "()"
      it "Empty quotes" $ pendingWith "Fix parser" -- cqlToSolr "\"\"" `shouldReturn` "()"
      it "Single quote" $ cqlToSolr "\"" `shouldThrow` anyException
      it "Quoted space" $ cqlToSolr "\" \"" `shouldReturn` "(\" \")"
    describe "Single search term" $ do
      it "Unquoted" $ cqlToSolr "dinosaur" `shouldReturn` "(dinosaur)"
      it "Unquoted with left space" $ pendingWith "Should this work?" -- cqlToSolr " dinosaur" `shouldReturn` "( dinosaur)"
      it "Unquoted with right space" $ pendingWith "Should this work?" -- cqlToSolr "dinosaur " `shouldReturn` "(dinosaur )"
      it "Quoted" $ cqlToSolr "\"dinosaur\"" `shouldReturn` "(dinosaur)"
      it "Quoted with left space" $
        cqlToSolr "\" dinosaur\"" `shouldReturn` "(\" dinosaur\")"
      it "Quoted with right space" $
        cqlToSolr "\"dinosaur \"" `shouldReturn` "(\"dinosaur \")"
      it "Parentesiezed" $ cqlToSolr "(dinosaur)" `shouldReturn` "((dinosaur))"
      it "Boolean" $ cqlToSolr "and" `shouldReturn` "(and)"
    describe "Multiple search term" $ do
      it "Unquoted" $ cqlToSolr "complete dinosaur" `shouldThrow` anyException
      it "Quoted" $
        cqlToSolr "\"complete dinosaur\"" `shouldReturn`
        "(\"complete dinosaur\")"
    describe "Indexes and search term" $ do
      it "With no spaces" $
        cqlToSolr "title=\"complete dinosaur\"" `shouldReturn`
        "(title:\"complete dinosaur\")"
      it "With spaces" $
        cqlToSolr "title = \"complete dinosaur\"" `shouldReturn`
        "(title:\"complete dinosaur\")"
      it "Schemad with no spaces" $
        cqlToSolr "dc.title=\"complete dinosaur\"" `shouldReturn`
        "(title:\"complete dinosaur\")"
      it "Schemad with spaces" $
        cqlToSolr "dc.title = \"complete dinosaur\"" `shouldReturn`
        "(title:\"complete dinosaur\")"
      it "cql.anyIndexes" $ pendingWith "How?" -- cqlToSolr "cql.anyIndexes = foobar" `shouldReturn` "(foobar)"
      it "cql.allIndexes" $
        cqlToSolr "cql.allIndexes = foobar" `shouldReturn` "(*:foobar)"
      it "cql.allRecords" $ pendingWith "How?" -- cqlToSolr "cql.allRecords = foobar" `shouldReturn` ""
      it "cql.anywhere" $ pendingWith "How?" -- cqlToSolr "cql.anywhere = foobar" `shouldReturn` ""
      it "cql.keywords" $ pendingWith "How?" -- cqlToSolr "cql.keywords = foobar" `shouldReturn` ""
      it "cql.resultSetId" $ pendingWith "How?" -- cqlToSolr "cql.resultSetId = foobar" `shouldReturn` ""
      it "cql.serverChoice" $ pendingWith "How?" -- cqlToSolr "cql.serverChoice = foobar" `shouldReturn` ""
      it "anyIndexes" $ pendingWith "How?" -- cqlToSolr "anyIndexes = foobar" `shouldReturn` "(foobar)"
      it "allIndexes" $
        cqlToSolr "allIndexes = foobar" `shouldReturn` "(*:foobar)"
      it "allRecords" $ pendingWith "How?" -- cqlToSolr "allRecords = foobar" `shouldReturn` ""
      it "anywhere" $ pendingWith "How?" -- cqlToSolr "anywhere = foobar" `shouldReturn` ""
      it "keywords" $ pendingWith "How?" -- cqlToSolr "keywords = foobar" `shouldReturn` ""
      it "resultSetId" $ pendingWith "How?" -- cqlToSolr "resultSetId = foobar" `shouldReturn` ""
      it "serverChoice" $ pendingWith "How?" -- cqlToSolr "serverChoice = foobar" `shouldReturn` ""
    describe "Sortby" $ do
      it "Index" $
        cqlToSolr "cat sortby title" `shouldReturn` "(cat)&sort=title desc,"
      it "Index (context)" $
        cqlToSolr "cat sortby dc.title" `shouldReturn` "(cat)&sort=title desc,"
    -- it "" $ show (P.parse cql "unknown" "cat sortby dc.title/sort.ascending") @?= ""
      it "Ascending" $
        cqlToSolr "cat sortby dc.title/sort.ascending" `shouldReturn`
        "(cat)&sort=title asc,"
      it "Descending" $
        cqlToSolr "cat sortby dc.title/sort.descending" `shouldReturn`
        "(cat)&sort=title desc,"
      it "Indexes (2)" $
        cqlToSolr "cat sortby dc.title dc.creator" `shouldReturn`
        "(cat)&sort=title desc,author desc,"
      it "Comparitor (=)" $
        cqlToSolr "title = cat sortby title" `shouldReturn`
        "(title:cat)&sort=title desc,"
      it "Quoted search term" $
        cqlToSolr "\"cat\" sortby dc.title" `shouldReturn`
        "(cat)&sort=title desc,"
    describe "Boolean groups" $ do
      it "Two terms (or)" $
        cqlToSolr "dinosaur or bird" `shouldReturn` "(dinosaur OR bird)"
      it "Two terms (and)" $
        cqlToSolr "dinosaur and bird" `shouldReturn` "(dinosaur AND bird)"
      it "Two terms (not)" $
        cqlToSolr "dinosaur not bird" `shouldReturn` "(dinosaur NOT bird)"
      it "Two terms (prox)" $ pendingWith "How?" -- cqlToSolr "dinosaur prox bird" `shouldReturn` ""
      it "Two terms (or) (case-insensitive)" $ pendingWith "Fix!" -- cqlToSolr "dinosaur Or bird" `shouldReturn` "(dinosaur OR bird)"
      it "Two terms (or) (case-insensitive)" $ pendingWith "Fix!" -- cqlToSolr "dinosaur oR bird" `shouldReturn` "(dinosaur OR bird)"
      it "Two terms (or) (case-insensitive)" $ pendingWith "Fix!" -- cqlToSolr "dinosaur OR bird" `shouldReturn` "(dinosaur OR bird)"
      it "Two terms parentesized (or)" $
        cqlToSolr "(dinosaur or bird)" `shouldReturn` "((dinosaur OR bird))"
      it "Three terms (and + not)" $
        cqlToSolr "dinosaur and bird or dinobird" `shouldReturn`
        "(dinosaur AND bird OR dinobird)"
      it "Two groups (and)" $
        cqlToSolr "(bird or dinosaur) and (feathers or scales)" `shouldReturn`
        "((bird OR dinosaur) AND (feathers OR scales))"
      it "Two groups parentesized (and)" $
        cqlToSolr "((bird or dinosaur) and (feathers or scales))" `shouldReturn`
        "(((bird OR dinosaur) AND (feathers OR scales)))"
      it "Left term missing" $
        cqlToSolr "not dinosaur" `shouldThrow` anyException
      it "Right term missing" $
        cqlToSolr "dinosaur not" `shouldThrow` anyException
    describe "Comparitors" $ do
      it "<" $
        cqlToSolr "publicationYear < 1980" `shouldReturn`
        "(extras_PublicationYear:[* TO 1980})"
      it "<=" $
        cqlToSolr "publicationYear <= 1980" `shouldReturn`
        "(extras_PublicationYear:[* TO 1980])"
      it ">" $
        cqlToSolr "publicationYear > 1980" `shouldReturn`
        "(extras_PublicationYear:{1980 TO *])"
      it ">=" $
        cqlToSolr "publicationYear >= 1980" `shouldReturn`
        "(extras_PublicationYear:[1980 TO *])"
      it "<>" $
        cqlToSolr "publicationYear <> 1980" `shouldReturn`
        "(-extras_PublicationYear:1980)"
      it "==" $ pendingWith "How?" -- cqlToSolr "publicationYear == 1980" `shouldReturn` ""
      it "= (number)" $
        cqlToSolr "publicationYear = 1980" `shouldReturn`
        "(extras_PublicationYear:1980)"
      it "= (single value) (unquoted) (text)" $
        cqlToSolr "title = gold" `shouldReturn` "(title:gold)"
      it "= (single value) (quoted) (text)" $
        cqlToSolr "title = \"gold\"" `shouldReturn` "(title:gold)"
      it "= (double value) (text)" $
        cqlToSolr "title = \"gold fish\"" `shouldReturn` "(title:\"gold fish\")"
      it "= (triple value) (text)" $
        cqlToSolr "title = \"gold fish bowl\"" `shouldReturn`
        "(title:\"gold fish bowl\")"
      it "within (single value) (unquoted)" $
        cqlToSolr "date within 2002" `shouldThrow` anyException
      it "within (single value) (quoted)" $
        cqlToSolr "date within \"2002\"" `shouldThrow` anyException
      it "within (double value)" $ pendingWith "Invalid Date String:'2002'"
        -- cqlToSolr "date within \"2002 2005\"" `shouldReturn`
        -- "(extras_PublicationTimestamp:[2002 TO 2005])"
      it "within (double value) (cased)" $
        pendingWith "Invalid Date String:'2002'"
        -- cqlToSolr "date WithIn \"2002 2005\"" `shouldReturn`
        -- "(extras_PublicationTimestamp:[2002 TO 2005])"
      it "within (triple value)" $
        cqlToSolr "date within \"2002 2005 2007\"" `shouldThrow` anyException
      it "encloses (single value) (unquoted)" $
        cqlToSolr "date encloses 2002" `shouldReturn`
        "((extras_TemporalCoverageBeginDate:[* TO 2002] AND extras_TemporalCoverageEndDate:[2002 TO *]))"
      it "encloses (single value) (quoted)" $
        cqlToSolr "date encloses \"2002\"" `shouldReturn`
        "((extras_TemporalCoverageBeginDate:[* TO 2002] AND extras_TemporalCoverageEndDate:[2002 TO *]))"
      it "encloses (double value)" $
        cqlToSolr "date encloses \"2002 2005\"" `shouldThrow` anyException
      it "encloses (triple value)" $
        cqlToSolr "date encloses \"2002 2005 2007\"" `shouldThrow` anyException
      it "any (single value) (unquoted)" $
        cqlToSolr "title any gold" `shouldReturn` "(title:(gold))"
      it "any (single value) (quoted)" $
        cqlToSolr "title any \"gold\"" `shouldReturn` "(title:(gold))"
      it "any (single value) (quoted) (cased)" $
        cqlToSolr "title Any \"gold\"" `shouldReturn` "(title:(gold))"
      it "any (double value)" $
        cqlToSolr "title any \"gold fish\"" `shouldReturn`
        "(title:(gold OR fish))"
      it "any (triple value)" $
        cqlToSolr "title any \"gold fish bowl\"" `shouldReturn`
        "(title:(gold OR fish OR bowl))"
      it "all (single value) (unquoted)" $
        cqlToSolr "title all gold" `shouldReturn` "(title:(gold))"
      it "all (single value) (quoted)" $
        cqlToSolr "title all \"gold\"" `shouldReturn` "(title:(gold))"
      it "all (single value) (quoted) (cased)" $
        cqlToSolr "title All \"gold\"" `shouldReturn` "(title:(gold))"
      it "all (double value)" $
        cqlToSolr "title all \"gold fish\"" `shouldReturn`
        "(title:(gold AND fish))"
      it "all (triple value)" $
        cqlToSolr "title all \"gold fish bowl\"" `shouldReturn`
        "(title:(gold AND fish AND bowl))"
      it "exact" $ pendingWith "How?" -- cqlToSolr "title exact \"the complete dinosaur\"" `shouldReturn` ""
    describe "Prefix assignment" $ do
      it "Index with space" $ pendingWith "How?" -- cqlToSolr "> dc = \"http://deepcustard.org/\" dc.custardDepth > 10" `shouldReturn` ""
      it "Index" $ pendingWith "How?" -- cqlToSolr ">dc=\"http://deepcustard.org/\" dc.custardDepth>10" `shouldReturn` ""
      it "> with space" $ pendingWith "How?" -- cqlToSolr "> \"http://deepcustard.org/\" custardDepth > 10" `shouldReturn` ""
      it ">" $ pendingWith "How?" -- cqlToSolr ">\"http://deepcustard.org/\" custardDepth>10" `shouldReturn` ""
    describe "Case insensitive" $ do
      it "Variating case (short)" $
        cqlToSolr "dC.tiTlE any fish" `shouldReturn` "(title:(fish))"
      it "Variating case (medium)" $
        cqlToSolr "dc.TitlE Any fish soRtbY Dc.TitlE" `shouldReturn`
        "(title:(fish))&sort=title desc,"
      it "Variating case (long)" $ pendingWith "How?" -- cqlToSolr "dc.TitlE Any/rEl.algOriThm=cori fish soRtbY Dc.TitlE" `shouldReturn` ""
    describe "Misc" $ do
      it "" $
        cqlToSolr "dc.title any fish or dc.creator any sanderson" `shouldReturn`
        "(title:(fish) OR author:(sanderson))"
      it "" $
        cqlToSolr "dc.title any fish or (dc.creator any sanderson)" `shouldReturn`
        "(title:(fish) OR (author:(sanderson)))"
      it "" $
        cqlToSolr
          "dc.title any fish or (dc.creator any sanderson and dc.identifier = \"id:1234567\")" `shouldReturn`
        "(title:(fish) OR (author:(sanderson) AND (extras_oai_identifier:\"id:1234567\" OR extras_DOI:\"id:1234567\")))"
      it "" $ pendingWith "How?" -- cqlToSolr "dc.title any fish prox / unit=word / distance>3 / relevant dc.title any squirrel" `shouldReturn` ""
      it "" $ pendingWith "How?" -- cqlToSolr "dc.title any fish prox/unit=word/distance>3/relevant dc.title any squirrel" `shouldReturn` ""
    describe "Solr" $ do
      it "answers" $
        solrRequest "http://136.172.11.43/solr/select" `shouldReturn` 200
      it "answers with JSON" $
        solrRequest "http://136.172.11.43/solr/select?wt=json" `shouldReturn`
        200
      it "answers with parsable JSON" $
        solrRequestJSON "http://136.172.11.43/solr/select?wt=json" `shouldReturn`
        0

cqlToSolr :: String -> IO String
cqlToSolr c = do
  req <- H.parseRequest "http://136.172.11.43/solr/select"
  let req' =
        H.setRequestQueryString
          ([("wt", Just "json"), ("rows", Just "0")] ++ args)
          req
  -- print req'
  res <- H.httpJSON req' -- (H.setRequestIgnoreStatus req')
  -- print res
  case H.getResponseStatusCode res of
    200 -> either (fail . show) return solrQuery
    _ -> do
      (print .
       maybe "Error message missing?" msg . Solr.error . H.getResponseBody)
        res
      fail "SolrSyntax error?"
  where
    solrQuery = either (fail . show) toSolrEither $ P.parse cql "(unknown)" c
    args =
      map
        ((\(f:s:_) -> (S8.pack f, Just (S8.pack s))) . LS.splitOn "=")
        (LS.splitOn "&" ("q=" ++ either (fail . show) Prelude.id solrQuery))
{-
Simple queries:

    dinosaur

    "complete dinosaur"
    title = "complete dinosaur"
    title exact "the complete dinosaur"

Queries using Boolean logic:

    dinosaur or bird

    Palomar assignment and "ice age"
    dinosaur not reptile
    dinosaur and bird or dinobird
    (bird or dinosaur) and (feathers or scales)
    "feathered dinosaur" and (yixian or jehol)

Queries accessing publication indexes:

    publicationYear < 1980

    lengthOfFemur > 2.4
    bioMass >= 100

Queries based on the proximity of words to each other in a document:

    ribs prox/distance<=5 chevrons

    ribs prox/unit=sentence chevrons
    ribs prox/distance>0/unit=paragraph chevrons

Queries across multiple dimensions:

    date within "2002 2005"
    dateRange encloses 2003

Queries based on relevance:

    subject any/relevant "fish frog"
    subject any/rel.lr "fish frog"

The latter example specifies using a specific algorithm for logistic regression.[2]
-}
